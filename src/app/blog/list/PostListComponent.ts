import {Component, OnInit} from '@angular/core';
import { PostService } from '../service/Postservice';
import { Observable } from 'rxjs';
import { PostListItem } from '../service/dataModel/PostListItem';

@Component({
    selector:'app-post-list',
    templateUrl: 'PostList.html'
})

export class PostListComponent implements OnInit{
    public postList: Observable<PostListItem[]>;
    constructor(private postService: PostService){

    }
    ngOnInit() {
        this.postList = this.postService.getAllPostItem();
    }
}